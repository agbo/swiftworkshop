
"Want more Swift? check out our"
"Swift for Objective C course at http://en.agbo.biz/tech/swift-for-objc-devs/"
" or the Spanish version at www.agbo.biz"

import UIKit

(2)
/////////////////////////////////
//  Basic Building blocks
//

// vars and cts
var size: Double = 40
let name = "Anakin"     // Type inference
//name = "Lucas"

// Everything is an Object (Show the std header)
Int.max
Double.abs(-1.3455)

// Conversions
let answer = Double(42) // calling a constructor
//let size = Int("33")  // Will cause an error. Later on we'll see how 
                        // to fix this


// Type Alias: will be important later to avoid going nuts
// typealias <newName> = <oldName>
typealias Integer = Int
var x1: Integer = Integer(3.1415926)

// Basic Collections
// String
var swift = "new language from Apple"
swift = swift + "!"     // Can add chars

// Array
// var list = [1,2,3,"wee"] // import foundation to fix it
var words = ["uno", "dos", "tres"]
words[2]    // access
words += ["cuatro"] // adding new members. might not work in previous 
                    // versions: words += "cuatro"


// Dictionary
var numberNames = [1:"one", 2:"two", 3:"three"]
//var d = ["Number" : "Value", 1:"Uno", 2: "Dos"]   // This only works if
                                                    // Foundation is imported. 
                                                    // It becomes a NSDictionary

numberNames[2]  // Returns {Some:"two"} WTF??? The actual value is encapsulated
                // into something. Later
//var rc = numberNames["Lucas Grijander"] // error
numberNames[8]  // returns nil, just like an NSDictionary 
                // (this is related with the WFT above)


// Iteration: similar to fast interation in ObjectiveC
var tally = ""
for element in words{
    tally =  "\(tally) \(element)"
}

for (key, value) in numberNames{
    println("\t\(key)\t\t\(value)")
}

// What on Earth is that (key, value) thing???
// Tuples: the simplest form of aggregate types (types that combine 
// primitive types together)
// We'll see others later on: structs, classes, and the funkiest
// ones of all: enums
// let's satrt with tuples and pattern matching (a very powerful technique 
// borrowed from Haskell)

var pair = (1, "one")   // This is not an array,it's actually a fancy way of 
                        // making structs
// Access
pair.0
pair.1

// They have type!
pair.0 = 33     // OK, I'm a 2-tuple with an int and a string
//pair.1 = 42   // Forget it, you won't stick your dirty Int into my String slot

// Great for creating short term association between types. For example for 
// returning multiple values from a function or metheods, all packed inside a
// tuple.
// Tuples are value objects, which means they are always copied when assigned
// and retuned. 
// These makes them an ideal lightweight container for sending information
// from one thread (or queue) to the other, while being sure not data is 
// actually among threads. Those who took my "Online Advanced iOS Programming"
// course are already well aware the dangers os sharing info between queues.



// One Last Thing!
// Iterate through rnages of numbers:

for i in Range(start: 40, end: 42){
    println(i)
}
// What's going on? We're creating a struct (another aggrega te type that we'll
// see later, with a lazy list groing from 40 to 41.
// There's syntactic sugar for it!
for i in 40...42{
    println(i)
}


("Back to Keynote")

/*----------------------------------------------------------------------------*/


(4)

/////////////////////////////////
//  Functions 101
//

"This is one of places where Swift really shines: it's almost as powerful as"
"some of the latest strict FP languages and it's pretty obvious"
"the creators of Swift have some admiration for Haskell"


// Functions have types too! A funcky ones, BTW.
// The type of this function is (Int)->Int
func f(a:Int)->Int{
    return a * a
}

// This is messy and I wish Apple had fixed their photocopiers before going
// after Haskell

// In haskell, this function would be:

// f a = a * a 
// No need to provide an type info. Haskell defualts to Num (a generic number
// type).
// Do you need to make it more specific? You may add type info, but it doesn't
// go in the same line, intermingled with the code:
// f :: Int -> Int
// f a = a * a 
// A clear separation of concerns, making the code much more readable.
// I'm filling a radar for this, and so should you.
// When we get to more complicated type declarations, you're gonna miss this
// Haskell feature. We'll try to aliviate our pain with typealias


// Parameter Naming
// Parameters can have 2 names: internal and external. This is commonly
// used to interface with ObjectiveC

// single internal name
func h(a: Int, b: Int) ->Int{
    return a * b
}
h(2,3)

// same internal and external name
func hh(#a: Int, #b: Int) -> Int{
    return a * b
}

hh(a: 42, b: 42)

// Independant internal and external names
// Good for interfacing with ObjC
func add(a:Int, b:Int, thenMultiplyBy c:Int) -> Int{
    return (a + b) * c
}
add(3, 4, thenMultiplyBy: 42)

// Parameters can have a default value, and when they do,
// then get an external name by default
func addSuffixTo(a:String, suffix :String = "ingly")->String{
    return a + suffix
}
addSuffixTo("accord")
addSuffixTo("Objective", suffix: "-C")


// Use common sense here, and make sure your code is always clear.
// Being clear always beats being cute or clever.

// No, you will not modify an argument!
// Unless you ask for permission
// This is not the same as passing something by ref
// No changes are done to the parameter

//func h´´´(var a:Int) ->Int{
//    a = 42 * a
//    return a * 3
//}


// InOut params: passing by ref
func swapInts(inout a: Int, inout b: Int){
    let aux = a
    a = b
    println()
    b = aux
}

var x = 3
var c = 1
swapInts(&c, &x)



////////////////////////////////////////////////////
// Enough of paramters, lets retunr something!
// Return Values

// By using tuples we can return several values
// Thanks to pattern matching (another idea picked up
// from Haskell) it's trivial to access these returned values

func namesOfNumbers(a:Int) -> (Int, String, String){
    
    var val:(Int, String, String)
    switch a{
    case 1:
        val = (1, "one", "uno")
    case 2:
        val = (2, "two", "dos")
    case 3:
        val = (3, "three", "tres")
    default:
        val = (a, "Go check Google translator!", "¡Vete a google translator!")
    }
    
    return val
}
// Assignement by pattern matching. Ignoring the first element
var (_, en, es) = namesOfNumbers(3)
en
es

("Back to Keynote")

/*----------------------------------------------------------------------------*/


(6)

/////////////////////////////////
//  High Order Functions
//  Functions unchained!
//

"Functions are types too, so we should be able to manipulate them as we"
"do with other types."
"Enter hight order functions!"

// Functions as parameters
// We will see this again in greater detail

typealias IntToIntFunc = (Int) -> Int
func apply(f:IntToIntFunc, n:Int) -> Int{
    
    return f(n)
}

func doubler(a:Int) ->Int{
    return a * 2
}

func add42(n:Int) -> Int{
    return n + 42
}

apply(doubler, 2)
apply(add42,42)

// Functions as return values
func compose(f:IntToIntFunc, g:IntToIntFunc) -> IntToIntFunc{
    
    func comp(a:Int) -> Int{
        return f(g(a))
    }
    return comp
}

var comp = compose(add42, doubler)
comp(8)

// Functions (of the same type) in an Array
var funcs = [doubler, add42, comp]
for each in funcs{
    each(42)
}

// You can treat functions as regular objects of the language, combine
// them, save them in collections, etc...
// We will check this later with more detail

("Back to Keynote")

/*----------------------------------------------------------------------------*/


(8)

/////////////////////////////////
//  Closures
//  Functions Reloaded!
//

"All functions are actully closures. That means blocks, in Objective C lingo"
"You already know them: they are first class objects that capture the lexical"
"environment where the where created"

func g(a:Int) -> Int{
    return a + 42
}
// is equivalent to:
let g = {(a:Int) -> Int in
    return a * 42}

// Notice that both can have the same name, but the constante will
// always have preference. I'm not sure if this is a feature or a bug
// but it allows to "Shadow" existing functions.
g(2)

// They capture the lexical environment: g was captured!
let gg = {(a:Int)->Int in g(g(a))}
gg(34)

// Just like the other functional representation, we can
// save them in collections and manipulate them as any other object
// Syntactic sugar for closures
var closures = [ g,
    {(a:Int) -> Int in a - 42},     // full syntax
    {a in return a - 42},           // no need for type as it inferes it from 
                                    // previous closures
    {a in a / 42},                  // no return when single line
    {$0 * 42}                       // no need for name vars
]


// Operators are also functions and therefore closures too!
typealias binaryFunc = (Int, Int) -> Int
var applier = { (f: binaryFunc, m:Int, n:Int) -> Int
                in
                return f(m,n) }

applier(*, 4,2)     // * is a binary function, just like +,-, /, etc...


// More syntactic sugar! Trailing closure
func applierInv(m:Int, n:Int, f:binaryFunc) -> Int{
    return applier(f, m,n)
}

var rc = applierInv(2, 4, {$0 * 2 + $1 + 3})
// could also be written as...
rc = applierInv(2, 4){$0 * 2 + $1 + 3}
// The usefullness of this becomes apparent when using completion 
// blocks in Cocoa

("Back to Keynote")

/*----------------------------------------------------------------------------*/


(10)

/////////////////////////////////
//  Optionals
//  The fear of nil
//


"Unlike ObjecitveC, accessing a nil value is not safe in Swift"
"Every object that might have a nil value at any point in time,"
"Must be wrapped inside a special type that acts as a container"
"for the 'normal' type and the fearsome nil"
"It's a special case of C's union, where you create a type that"
"can wrap different types, but only one at a time"
"This special wrapping type is called an Optional"

// Boxing something inside an optionl
var maybeAString:String? = "I'm boxed!"   // |String|Nil|
var maybeAnInt: Int?

// Unwrapping.
// Safe way: combines test with assignment to a non optional type
if let certainlyAString = maybeAString{
    println(certainlyAString)
}

// Roque way: better know what you're doing, or else runtime error!
println(maybeAString!)
//println(maybeAnInt!) // Will cause runtime error




("Back to Keynote")

/*----------------------------------------------------------------------------*/


(12)

/////////////////////////////////////////////
//  Decorators
//  Functions that transform functions
//
//

"This is a very common pattern in other languages, such as Python."
"A decorator is a function that takes a function and returns a modified"
"version of it. Useful to abstract away ugly and complex edge cases that"
"might be added to your functions when needed:"
"logging, memoization, retrying network operations in case of failure,"
"special debuging behavior, etc..."

// Logging calls
func logger(f:IntToIntFunc) -> IntToIntFunc{
    
    return{
        (n: Int) -> Int in
        var r = f(n)
        var moment = NSDate()
        var fmt = NSDateFormatter()
        fmt.dateStyle = NSDateFormatterStyle.LongStyle
        println("Called at \(fmt.stringFromDate(moment))\nInput \(n) -> \(r)")
        return r
    }
}

logger(g)(88)

// Adding pre (or post) conditions
func precond(f: IntToIntFunc,
    cond: @autoclosure ()->Bool) -> IntToIntFunc{
    
        return{
            (n: Int) -> Int in
            var res = cond()
            assert(res, "Precondition not met. Tough luck.")
            return f(n)
        }
}
//precond(g, size < 10)(33)     // this drops us to the debugger. 




("Back to Keynote")

/*----------------------------------------------------------------------------*/


(17)

/////////////////////////////////
//  Aggregate types
//  Properties
//  Methods
//  Initializers
//  Structs, Enums, Classes
//

// A "normal" enum
enum LightSabreColor: Int{
    case Red, Blue, Green, Purple
}


struct LightSabre{
    // "class" var
    static let quote = "An elegant weapon for a more civilized age"
    
    var color : LightSabreColor = .Blue {   // stored prop. with observer
        willSet(newValue){
            println("About to change \(color) to \(newValue)")
        }
    }
    
    var isDoubleBladed = false

}

class Jedi {    // From what is it inheriting from??? SwiftObject (hidden)
    var lightSabre = LightSabre(color: .Blue, isDoubleBladed: false)
    
    var name : String?
    var padawanOf: Jedi?    // what if a you have a masterOf property and a causes strong cycle?
    var midiclorians = 10
    
    // computed property
    var fullName: String{
        get{
            var full = ""
            if let theName = name{
                full = theName
            }
            
            if let master = padawanOf{
                full = full + " padawan of \(master)"
            }
            return "Master \(full) (OJT)"
        }
    }
    
    // Initializers
    // Designated
    init(name : String?, padawanOf: Jedi?){
        self.name = name        // explain how self works
        self.padawanOf = padawanOf
    }
    
    init(){
        name = nil
        padawanOf = nil
    }
    
    // Convenience
    convenience init(name:String){
        self.init(name:name, padawanOf:nil)
    }
    
    // Regular methods
    func totalMidichlorians()->Int{
        var total = midiclorians
        
        // Optional chaining
        if let parentalMidichlorians = padawanOf?.midiclorians{
            total = midiclorians + parentalMidichlorians
        }
        return total
    }
}

var anakin = Jedi()
var kenobi = Jedi()
anakin.padawanOf = kenobi
kenobi.midiclorians = 455
anakin.totalMidichlorians()


("Back to Keynote")

/*----------------------------------------------------------------------------*/


(19)

/////////////////////////////////
//  Inheritance
//  Overriding
//
//


"Single inheritance, but plentiful of protocols. Most cool things in"
"Swift are implemented via protocols. Protocols are also one of the main"
"ways to extend and adapt the language to your needs"

class Sith: Jedi {
    
    // if we don't write any designated initializers,
    // we inherit those from Jedi
    
    override init(){
        super.init()
        lightSabre.color = .Red
        lightSabre.isDoubleBladed = true
    }
    
    override init(name: String?, padawanOf: Jedi?) {
        super.init(name: name, padawanOf: padawanOf)
    }
    
    convenience init(name: String?) {
        self.init(name: name, padawanOf: nil)
    }
    
    // overriding a property
    override var fullName: String{
        get{
            var full = ""
            if let theName = name{
                full = theName
            }
            if let master = padawanOf{
                full = "\(full) padawan of \(master)"
            }
            return "\(full) DLS (Dark Lord of the Sith)"
        }
    }

    // Observing an inherited property
    override var lightSabre: LightSabre{
        willSet(newValue){
            println("About to set a new lightSabre: \(newValue)")
        }
        didSet{
            println("just go a brand new LightSabre!")
        }
    }

}

var darthVader = Sith(name: "Darth Vader", padawanOf:kenobi)



("Back to Keynote")

/*----------------------------------------------------------------------------*/


(21, "OPTIONAL")

/////////////////////////////////
//  Type casting
//  optional
//
//

"is behaves as isMemberOfClass. There's no isKindIfClass"
"as makes a rogue cast. as? does it safely"
"AnyObject stands for... any instance of a class."
"Any is just any type. Both are used to interface with Objective C"

if kenobi is Sith {
    println("No way!")
}

anakin = darthVader as Jedi     // Up cast. Pretty safe
// var noo = kenobi as Sith     // causes an error, there's no way to 
                                // downcast a Jedi to a Sith 
                                // (with a single infamous exception)

// To avoid these errors in runtime, we should always use the safe operator
// as?
if let never = (kenobi as? Sith) {
    println("Obi Wan NEVER gave himself to the Dark Side!!!")
}



("Back to Keynote")

/*----------------------------------------------------------------------------*/


(23, "OPTIONAL")

/////////////////////////////////
//  Nested types
//  not what you're thinking...
//
//

"These are types defined within the definition of another one."
"These ARE NOT JAVA'S INNER CLASSES!"
"They are globaly visible, by prepending the name of the container type"
"They do NOT have access to the lexical environment in which they"
"where defined! Unlike closures."

class Dagobah {
    
    var environment = "Full of slime and lizards"
    
    class Yoda {
        
        func complain() -> String{
            //return "This place is \(environment)" // won't compile
            return "This planet sucks eggs!!!"
        }
    }
}

// Creating an instance of Yoda is possible, it's not hidden
var greenAndMean = Dagobah.Yoda()


// Inner types might be a foundation for lightweight modules


("Back to Keynote")

/*----------------------------------------------------------------------------*/


(24)

/////////////////////////////////
//  Extensions 101
//  How the geeky Alice went 
//  down the Rabbit Hole...
//

"Extensions are similar to ObjectiveC's categories. Unlike categories,"
"extensions cannot break inheritance, no matter how you try. They also have"
"no names."
"Extensions are used extensively ;-) in Swift. They provide a way to group"
"common features of a class, struct or enum"

// Adding properties to create a new type, the Euro
typealias Euro = Float

extension Euro{
    var € : Float {return self}
    var $ : Float {return self * 1.29}
}

var totalAmount = 1000.€ + 3452.99.$

// Adding methods to existing types to create new
// control structures

typealias Task = ()->()
extension Int{
    func times(task: Task){
        for i in 1...self{
            task()
        }
    }
}
4.times({println("I am Groot")})

// using the trailing closure syntactic sugar:
42.times{
    println("Bring a towel!")
}



("Back to Keynote")

/*----------------------------------------------------------------------------*/


(26)

///////////////////////////////////////
//  Protocols
//  The keys to the walled garden...
//  Lo vemos todo con los generadores




/*----------------------------------------------------------------------------*/


(30)

//////////////////////////////////////////
//  Generators
//  Generating complex data one step at
//  a time, since 1977
//

struct EvenNumbers: GeneratorType{
    typealias Element = Int   // not necessary, the compiler should find it out
    
    var current = 0
    mutating func next() -> Int?{
        current = current + 2
        return current
    }
    
}

var evens = EvenNumbers()
evens.next()
evens.next()

struct Fibonacci: GeneratorType{
    
    var (previous, current) = (0,0)
    
    mutating func next() -> Int? {
        var next: Int
        if previous == 0 && current == 0 {
            next = 1
            current = 1
        }else{
            next = previous + current
            (previous, current) = (current, next)
            
        }
        return next
    }
}
var fib = Fibonacci()
fib.next()
fib.next()
fib.next()
fib.next()



struct CommandlineArgs: GeneratorType{
    var args = Process.arguments
    var current = 0
    
    mutating func next() -> String? {
        if args.isEmpty || current == args.endIndex{
            return nil
        }else{
            var rc = args[current]
            current++
            return rc
        }
    }
}

// Gotta wait until we can use Swift on OSX again.
// Thanks, Apple...





/*----------------------------------------------------------------------------*/


(38)

//////////////////////////////////////////
//  Handling sequences functional way
//
//


var s = ["uno", "dos", "tres", "cuatro", "cinco", "seis"]

var ss = s.filter{$0.hasSuffix("s")}
ss
var lengths = ss.map{countElements($0)}
lengths
var total = lengths.reduce(0, combine: {a,b in a + b})
total

// or...
s.filter{$0.hasSuffix("s")}
    .map{countElements($0)}
    .reduce(0, combine:{a,b in a + b})

// countElements() is a global generic function.
// This is totally different from ObjectiveC and closer
// to the way C++ and the STL works.






/*----------------------------------------------------------------------------*/


(40)

//////////////////////////////////////////
//  Access Control
//  Not what you're expecting...
//


class ExtraterrestrialPlant {
    private func speak() -> String{
        return "I am Groot"
    }
}
var b = ExtraterrestrialPlant()
b.speak()   // Not wah you were expecting, huh?

// Simulating hidden behavior 

// External interface that everyone can see
protocol Groot{
    var speak: String {get set}
}


func grootMaker() -> Groot{
    
    class TheGrootWithin: Groot{
        var hiddenValue = "I am Groot"
        
        var speak: String {
            get{
                return topSecretGrootifier(hiddenValue)
            }
            set{
                hiddenValue = newValue
            }
        }
        func topSecretGrootifier(n: String) -> String{
            return hiddenValue + "!!"
        }
    }
    
    return TheGrootWithin()
}

// hiddenValue and topSecretGrootifier are hidden, while only the
// public protocol is visible.
// Same technique used by Cocoa with Class Clusters: NSString, etc...

var groot = grootMaker()
// groot.hiddenValue
groot.speak

/*----------------------------------------------------------------------------*/

"Thanks for making it this far! Want more Swift? check out our"
"Swift for Objective C course at http://en.agbo.biz/tech/swift-for-objc-devs/"
" or the Spanish version at www.agbo.biz"







